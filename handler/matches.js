const matchCommand = require("../command/matchCommand.js")

module.exports.harvest = async (request, context, callback) => {
  try {
    console.debug("received request: matches/${accountId}",request)
    const accountId = parseInt(request.pathParameters.accountId);
    if(request.queryStringParameters)
    {
      var startTime = (request.queryStringParameters.startTime)?parseInt(request.queryStringParameters.startTime):undefined;
      var endTime = (request.queryStringParameters.endTime)?parseInt(request.queryStringParameters.endTime):undefined;
    }
    var matchData = await matchCommand.fetchMatchList(accountId,startTime,endTime);
    const response = {
      statusCode:200 ,
      body: JSON.stringify(matchData)
    }
    callback(null, response);
  } catch(error) {
    const response = {
      statusCode:500,
      body: JSON.stringify({message: "An error occurred", error:JSON.stringify(error)})
    }
    console.log(`An error occurred!:`,error);
    callback(null,response);
  }
};
