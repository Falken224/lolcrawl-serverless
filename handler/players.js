const playerCommand = require("../command/playerCommand.js")

module.exports.byQuery = async (request, context, callback) => {
  try {
    console.log("received request: players/query",request);
    const playerName = request.queryStringParameters.name;
    var playerData = await playerCommand.getPlayerByName(playerName);
    const response = {
      statusCode:200 ,
      body: JSON.stringify(playerData)
    }
    callback(null, response);
  } catch(error) {
    const response = {
      statusCode:500,
      body: JSON.stringify({message: "An error occurred", error:error})
    }
    console.log(`An error occurred!:`,error);
    callback(response,null);
  }
};

module.exports.get = async (request, context, callback) => {
  try {
    console.log("received request: players/${accountId}",request);
    const accountId = parseInt(request.pathParameters.accountId);
    var playerData = await playerCommand.getPlayerByAccountId(accountId);
    const response = {
      statusCode:200 ,
      body: JSON.stringify(playerData)
    }
    callback(null, response);
  } catch(error) {
    const response = {
      statusCode:500,
      body: JSON.stringify({message: "An error occurred", error:error})
    }
    console.log(`An error occurred!:`,error);
    callback(response,null);
  }
};
