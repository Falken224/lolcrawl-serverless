module.exports = {
	harvestRateLimit,
	fetchDelayUntilAllowed,
	rateLimitFromHeader,
	fetchRateLimitFromDB
}

async function harvestRateLimit(methodName,headers,dynamodb)
{
	var globalTotal = headers['x-app-rate-limit'];
	var globalCount = headers['x-app-rate-limit-count'];
	var methodTotal = headers['x-method-rate-limit'];
	var methodCount = headers['x-method-rate-limit-count'];

	console.log("Completed call to "+methodName+" - New Counts:");
	console.log("-- global "+globalCount+" of "+globalTotal);
	console.log("-- method "+methodCount+" of "+methodTotal);

	var globalRL = rateLimitFromHeader(globalCount,globalTotal,"GLOBAL");
	var localRL = rateLimitFromHeader(methodCount,methodTotal,methodName);

	await Promise.all([
		saveRateLimitToDB(globalRL,dynamodb),
		saveRateLimitToDB(localRL,dynamodb)
	]);
};

async function fetchDelayUntilAllowed(methodName,dynamodb)
{
	const [globalrl,methodrl] = await Promise.all([
		fetchRateLimitFromDB("GLOBAL",dynamodb),
		fetchRateLimitFromDB(methodName,dynamodb)
	]);

	var now = new Date();

	var finalWaitDuration = 0;

	if(globalrl)
	{
		for(timespan in globalrl.periodMax)
		{
			console.debug(`GLOBAL - Period: ${timespan} - Count: ${globalrl.periodCount[timespan]} - Max: ${globalrl.periodMax[timespan]}`);
			if(globalrl.periodCount[timespan]>=globalrl.periodMax[timespan])
			{
				console.warn(`GLOBAL - LIMITED - ${timespan} seconds - checking delay.`);
				var resetTime;
				if(globalrl.resetTime[timespan])
				{
					resetTime = new Date(Date.parse(globalrl.resetTime[timespan]));
				} else {
					resetTime = new Date(Date.parse(globalrl.updatedTime));
					resetTime.setSeconds(resetTime.getSeconds()+parseInt(timespan));
				}
				console.debug(`Reset time is${resetTime}`);
				var waitDuration = (resetTime-now)/1000;
				if(waitDuration<0)
				{
					console.warn(`GLOBAL ${timespan} second window - No-Limit - reset time has passed.`);
					continue;
				}
				console.warn(`GLOBAL - LIMITED - waiting for ${waitDuration} seconds`);
				if(waitDuration > finalWaitDuration)
				{
					finalWaitDuration = waitDuration;
				}
			}
		}
	}

	if(methodrl)
	{
		for(var timespan in methodrl.periodMax)
		{
			console.debug(`Method ${methodName} - Period: ${timespan} - Count: ${methodrl.periodCount[timespan]} - Max: ${methodrl.periodMax[timespan]}`);
			if(methodrl.periodCount[timespan]>=methodrl.periodMax[timespan])
			{
				console.warn(`METHOD ${methodName} - LIMITED - ${timespan} seconds - checking delay.`);
				var resetTime;
				if(methodrl.resetTime[timespan])
				{
					resetTime = new Date(Date.parse(methodrl.resetTime[timespan]));
				} else {
					resetTime = new Date(Date.parse(methodrl.updatedTime));
					resetTime.setSeconds(resetTime.getSeconds()+parseInt(timespan));
				}

				console.debug(`Reset time is${resetTime}`);
				var waitDuration = (resetTime-now)/1000;
				if(waitDuration<0)
				{
					console.warn(`Method ${methodName} ${timespan} second window - No-Limit - reset time has passed.`);
					continue;
				}
				console.warn(`METHOD ${methodName} - waiting for ${waitDuration} seconds`);
				if(waitDuration > finalWaitDuration)
				{
					finalWaitDuration = waitDuration;
				}
			}
		}
	}
	return finalWaitDuration;
}

function rateLimitFromHeader(countHeader, maxHeader, rateType)
{
	console.log(`Harvesting rate limits for max:${maxHeader} count:${countHeader} rateType:${rateType}`);
	var rateLimit = {
		rateType: rateType,
		periodMax: {},
		periodCount: {},
		resetTime: {},
		updatedTime: new Date().toISOString()
	};
	countHeader.split(",").forEach(entry=>{
		var pair = entry.split(":")
		rateLimit.periodCount[pair[1]]=parseInt(pair[0]);
	});
	maxHeader.split(",").forEach(entry=>{
		var pair = entry.split(":")
		rateLimit.periodMax[pair[1]]=parseInt(pair[0]);
		if(rateLimit.periodCount[pair[1]]==pair[0]-1)
		{
			//If our remining count is 1 less than the max, then we JUST started our quota
			rateLimit.resetTime[pair[1]]=rateLimit.updatedTime;
		}
	});
	return rateLimit;
};

async function saveRateLimitToDB(rateLimit,dynamodb)
{
	console.log("Creating save params");
	const params = {
		TableName: "lolcrawl-ratelimit",
		Key: "rateType",
		Item: rateLimit,
		ConditionExpression: "(attribute_not_exists(rateType)) OR (updatedTime < :new_updatedTime)",
		ExpressionAttributeValues: {
			":new_updatedTime" : rateLimit.updatedTime
		}
	}

	try {
		const data = await dynamodb.put(params).promise();
		console.debug(`Successfully saved rate limit.`,data);
		return data;
	} catch (err) {
		console.error(`Failed to put rate limit into DynamoDB: ${rateLimit}:`,err);
		throw err;
	}
}

async function fetchRateLimitFromDB(rateType,dynamodb)
{
	console.log("Creating fetch params");
	const params = {
		TableName: "lolcrawl-ratelimit",
		Key: { "rateType": rateType }
	};

	try{
		const data = await dynamodb.get(params).promise();
		console.debug(`Successfully fetched rate limit.`,data);
		return data.Item;
	} catch(err){
		console.error(`Failed to fetch rate limit ${rateType} from DynamoDB:`,err);
		throw err;
	}
}
