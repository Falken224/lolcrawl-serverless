function define(name, value) {
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true
    });
}

define("RIOT_API_URL_BASE", "https://na1.api.riotgames.com/lol/");
define("RIOT_API_KEY", process.env.RIOT_API_KEY);
