const expect = require('chai').expect;
const rateLimit = require('../common/rateLimit.js');

describe('rateLimitFromHeader()',()=>{
  const maxHeader = "100:1,100000:60";
  const countHeader = "99:1,8999:60";

  const testRL = rateLimit.rateLimitFromHeader(countHeader,maxHeader,'test-RL-type');

  it('should have the correct type',()=>{
    expect(testRL.rateType).to.be.equal('test-RL-type');
  });
  it('should have a valid updatedTime',()=>{
    expect(testRL.updatedTime).to.not.be.undefined;
  });

  it('should contain the correct max/count values for the 60 minute limit',()=>{
    expect(testRL.periodMax[60]).to.be.equal(100000);
    expect(testRL.periodCount[60]).to.be.equal(8999);
  });

  it('should contain the correct max/count values for the 1 minute limit',()=>{
    expect(testRL.periodMax[1]).to.be.equal(100);
    expect(testRL.periodCount[1]).to.be.equal(99);
  });

  it('should contain a correct resetTime for the 1 minute limit',()=>{
    expect(testRL.resetTime[1]).to.be.equal(testRL.updatedTime);
  });

  it('should not contain resetTime for the 60 minute limit',()=>{
    expect(testRL.resetTime[60]).to.be.undefined;
  });
});

//TODO: Add unit test for the harvest method as well.

describe('fetchDelayUntilAllowed()', ()=>{
  it('should reject global rate limited method calls.',async ()=>{
    var globalRL = buildBaseRL("GLOBAL");
    //Exceed the global RL.
    var resetDate = new Date();
    resetDate.setSeconds(resetDate.getSeconds()+20);
    globalRL.resetTime['60']=resetDate.toISOString();
    globalRL.periodCount['60']=20001;

    var methodRL = buildBaseRL("test-method");
    var dynamomock = buildDynamoMock([globalRL,methodRL]);
    var waitTime = await rateLimit.fetchDelayUntilAllowed("test-method",dynamomock);
    expect(waitTime).to.be.above(19);
  });

  it('should reject method-level rate limited calls.',async ()=>{
    var globalRL = buildBaseRL("GLOBAL");
    var methodRL = buildBaseRL("test-method");
    //Exceed the global RL.
    var resetDate = new Date();
    resetDate.setSeconds(resetDate.getSeconds()+20);
    methodRL.resetTime['60']=resetDate.toISOString();
    methodRL.periodCount['60']=20001;

    var dynamomock = buildDynamoMock([globalRL,methodRL]);
    var waitTime = await rateLimit.fetchDelayUntilAllowed("test-method",dynamomock);
    expect(waitTime).to.be.above(19);
  });

  it('should accept method invocations when other methods are rate limited',async ()=>{
    var globalRL = buildBaseRL("GLOBAL");
    var method1RL = buildBaseRL("test-method1");
    var method2RL = buildBaseRL("test-method2");
    //Exceed the global RL.
    var resetDate = new Date();
    resetDate.setSeconds(resetDate.getSeconds()+20);
    method2RL.resetTime['60']=resetDate.toISOString();
    method2RL.periodCount['60']=20001;

    var dynamomock = buildDynamoMock([globalRL,method1RL,method2RL]);
    var waitTime = await rateLimit.fetchDelayUntilAllowed("test-method1",dynamomock);
    expect(waitTime).to.be.equal(0);
  });

  it('should return the longest of all wait intervals when multiple windows are limited.',async ()=>{
    var globalRL = buildBaseRL("GLOBAL");
    var methodRL = buildBaseRL("test-method");
    //Set up all the intervals.
    var resetDate = new Date();

    //global 1 second gets a 20 second delay.
    resetDate.setSeconds(resetDate.getSeconds()+30);
    globalRL.resetTime['1']=resetDate.toISOString();
    globalRL.periodCount['1']=1001;

    //global 60 second gets an additional 2 hour delay.
    resetDate.setHours(resetDate.getHours()+2);
    globalRL.resetTime['60']=resetDate.toISOString();
    globalRL.periodCount['60']=20001;

    //method level ones both get a +1hr and 30 second delay.
    resetDate.setHours(resetDate.getHours()-1);
    methodRL.resetTime['1']=resetDate.toISOString();
    methodRL.periodCount['1']=1001;
    methodRL.resetTime['60']=resetDate.toISOString();
    methodRL.periodCount['60']=20001;

    var dynamomock = buildDynamoMock([globalRL,methodRL]);
    var waitTime = await rateLimit.fetchDelayUntilAllowed("test-method",dynamomock);
    expect(waitTime).to.be.above(3700); //The second longest time should be 3630 exactly.
  });

  it('should return the "last updated time" plus the time window if no reset is present for a limited window.',async()=>{
    var globalRL = buildBaseRL("GLOBAL");
    var methodRL = buildBaseRL("test-method");

    methodRL.periodCount['60']=20001;
    var lastUpdateDate = new Date(Date.parse(methodRL.updatedTime));
    lastUpdateDate.setSeconds(lastUpdateDate.getSeconds()-30);
    methodRL.updatedTime = lastUpdateDate.toISOString();

    var dynamomock = buildDynamoMock([globalRL,methodRL]);
    var waitTime = await rateLimit.fetchDelayUntilAllowed("test-method",dynamomock);
    expect(waitTime).to.be.above(29);
    expect(waitTime).to.be.below(31);
  });

  it('should return 0 delay if no rate limit entries are present.',async()=>{
    var dynamomock = buildDynamoMock([]);
    var waitTime = await rateLimit.fetchDelayUntilAllowed("test-method",dynamomock);
    expect(waitTime).to.be.equal(0);
  });
})

function buildBaseRL(rateType){
  return {
    rateType: rateType,
		periodMax: {
      '1' : 1000,
      '60' : 20000
    },
		periodCount: {
      '1' : 1,
      '60' : 1
    },
		resetTime: {},
		updatedTime: new Date().toISOString()
  };
}

function buildDynamoMock(listOfRateLimits)
{
  return {
    get : function(params){
      return {
        promise: ()=>{
          return new Promise((resolve,reject)=>{
            try{
              var pickedRL = listOfRateLimits.filter(rl=>rl.rateType==params.Key.rateType).pop();
              if(pickedRL)
              {
                resolve({Item : pickedRL});
              } else {
                resolve({});
              }
            } catch(error) {
              reject(error);
            }
          });
        }
      }
    },
    put : function(params){}
  }
}
