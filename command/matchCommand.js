const riotData = require("./riotData.js");
const playerCommand = require("./playerCommand.js");
const AWS = require("aws-sdk");

const dynamodb = new AWS.DynamoDB.DocumentClient();

module.exports = {
	fetchMatchList
}

async function fetchMatchList(accountId,startDate,endDate)
{
	let playerData = await playerCommand.getPlayerByAccountId(accountId);
	if(!endDate)
	{
		endDate=new Date();
		if(startDate)
		{
			endDate.setDate(startDate.getDate()+7);
		}
	}

	if(!startDate)
	{
		startDate=new Date();
		startDate.setDate(endDate.getDate()-7);
	}

	try{
		let startIndex=0;
		let maxIndex=0;
		let allMatches = new Array();
		do {
			let matchListData = await riotData.fetchMatchHistory(accountId,startDate,endDate,startIndex,dynamodb);
			maxIndex = matchListData.totalGames;
			startIndex = matchListData.endIndex;
			allMatches = allMatches.concat(matchListData.matches);
		}while(startIndex < maxIndex);
		await Promise.all(allMatches.map(game=>saveGameInfo(game)));
		await updateFetchDataForPlayer(
			playerData,
			Math.min(...allMatches.map(game=>game.gameId)),
			Math.max(...allMatches.map(game=>game.gameId)),
			Math.min(...allMatches.map(game=>game.timestamp)),
			Math.max(...allMatches.map(game=>game.timestamp))
		);
		return allMatches;
	}catch(err){
		throw err;
	}
}

async function saveGameInfo(gameInfo)
{
  console.log("Creating save params");
	const params = {
		TableName: "lolcrawl-games",
		Key: "gameId",
		Item:{
			gameId: gameInfo.gameId,
			platformId: gameInfo.platformId,
			timestamp: gameInfo.timestamp,
			queue: gameInfo.queue,
			season: gameInfo.season,
			fetched: false
		},
		ConditionExpression: "attribute_not_exists(gameId)"
	}

	try {
		const gameData = await dynamodb.put(params).promise();
		console.debug(`Successfully saved game info.`,JSON.stringify(gameData));
		return gameData;
	} catch (err) {
		if(err.code=="ConditionalCheckFailedException"){
			console.debug(`Game ${gameInfo.gameId} already exists.`);
			return;
		}
		console.error(`Failed to save game: ${gameInfo.gameId}:`,err);
		throw err;
	}
}

async function updateFetchDataForPlayer(playerData, minGame, maxGame, minTime, maxTime)
{
	let UpdateExpression = "set";
	let ExpressionAttributeValues = {};

	let isUpdated = false;

	if(playerData.minGameFetched==-1 || playerData.minGameFetched>minGame)
	{
		UpdateExpression+=" minGameFetched=:minGame"
		ExpressionAttributeValues[':minGame']=minGame;
		isUpdated=true;
	}
	if(playerData.maxGameFetched==-1 || playerData.maxGameFetched<maxGame)
	{
		UpdateExpression+=`${isUpdated?',':''} maxGameFetched=:maxGame`
		ExpressionAttributeValues[':maxGame']=maxGame;
		isUpdated=true;
	}
	if(playerData.minTimeFetched==-1 || playerData.minTimeFetched>minTime)
	{
		UpdateExpression+=`${isUpdated?',':''} minTimeFetched=:minTime`
		ExpressionAttributeValues[':minTime']=minTime;
		isUpdated=true;
	}
	if(playerData.maxTimeFetched==-1 || playerData.maxTimeFetched<maxTime)
	{
		UpdateExpression+=`${isUpdated?',':''} maxTimeFetched=:maxTime`
		ExpressionAttributeValues[':maxTime']=maxTime;
		isUpdated=true;
	}

	if(!isUpdated) return playerData;

	console.log(`Saving new playerData: `, UpdateExpression, ExpressionAttributeValues);
	const params = {
		TableName: "lolcrawl-accounts",
		Key: { "accountId": playerData.accountId },
		UpdateExpression,
		ExpressionAttributeValues,
		ReturnValues: "UPDATED_NEW"
	};

	try{
		const newPlayerData = await dynamodb.update(params).promise();
		console.debug(`Successfully update player fetch info.`,newPlayerData.Item);
		return newPlayerData.Item;
	} catch(err){
		console.error(`Failed to update player fetch data:`,err);
		throw err;
	}
}
