const request = require('request-promise');
const {constants,rateLimit} = require("../common/index.js");

module.exports = {
  fetchSummonerByName,
  fetchSummonerByAccountId,
  fetchMatchHistory
}

async function fetchSummonerByName(dynamodb,summonerName){
		const methodName = "summoner/v3/summoners/by-name/";
    const callDelay = await rateLimit.fetchDelayUntilAllowed(methodName,dynamodb);

    var headers;
    //no action right now if callDelay>0  We'll just bail.
		try {
        if(callDelay!=0) throw new Error(`Rate limit exceeded.  Must wait for ${callDelay} seconds.`);
        var response = await request({
        	"method":"GET",
        	"uri":constants.RIOT_API_URL_BASE+methodName+summonerName,
        	"resolveWithFullResponse" : true,
        	"json": true,
        	"headers": {
        		"X-Riot-Token":constants.RIOT_API_KEY
        	}
        });
        console.debug(response);
        headers=response.headers;
        return response.body;
    } catch(error) {
        console.error(error);
        if(error.headers)
        {
          headers = error.headers;
        }
        throw new Error(`Failed to fetch summoner ${summonerName} by name.`,error);
    } finally {
        if(headers)
        {
          await rateLimit.harvestRateLimit(methodName,headers,dynamodb);
        }
    }
}

async function fetchSummonerByAccountId(dynamodb,accountId){
		const methodName = "summoner/v3/summoners/by-account/";
    const callDelay = await rateLimit.fetchDelayUntilAllowed(methodName,dynamodb);

    var headers;
    //no action right now if callDelay>0  We'll just bail.
		try {
        if(callDelay!=0) throw new Error(`Rate limit exceeded.  Must wait for ${callDelay} seconds.`);
        var response = await request({
        	"method":"GET",
        	"uri":constants.RIOT_API_URL_BASE+methodName+accountId,
        	"resolveWithFullResponse" : true,
        	"json": true,
        	"headers": {
        		"X-Riot-Token":constants.RIOT_API_KEY
        	}
        });
        console.debug(response);
        headers=response.headers;
        return response.body;
    } catch(error) {
        console.error(error);
        if(error.headers)
        {
          headers = error.headers;
        }
        throw new Error(`Failed to fetch account ${accountId}.`,error);
    } finally {
        if(headers)
        {
          await rateLimit.harvestRateLimit(methodName,headers,dynamodb);
        }
    }
}

async function fetchMatchHistory(accountId,startDate,endDate,index,dynamodb){
		const methodName = "match/v3/matchlists/by-account/";
    if(!index) index=0;

    var headers;
    const callDelay = await rateLimit.fetchDelayUntilAllowed(methodName,dynamodb);
    try {
        if(callDelay!=0) throw new Error(`Rate limit exceeded.  Must wait for ${callDelay} seconds.`);
			  var response = await request({
  				"method" : "GET",
  				"uri" : constants.RIOT_API_URL_BASE+methodName+accountId+
              "?beginTime="+startDate.getTime()+
              "&endTime="+endDate.getTime()+
              "&beginIndex="+index,
  				"resolveWithFullResponse" : true,
  				"json" : "true",
  				"headers": {
  					"X-Riot-Token":constants.RIOT_API_KEY
  				}
  			});
        console.debug(response);
        headers=response.headers;
        return response.body;
    } catch(error) {
        console.error(error);
        if(error.headers)
        {
          headers = error.headers;
        }
        throw new Error(`Failed to fetch match history ${accountId}-${index}.`,error);
    } finally {
        if(headers)
        {
          await rateLimit.harvestRateLimit(methodName,headers,dynamodb);
        }
    }
}
