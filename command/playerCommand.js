const riotData = require("./riotData.js")
const AWS = require("aws-sdk");

const dynamodb = new AWS.DynamoDB.DocumentClient();

module.exports = {
	getPlayerByName,
	getPlayerByAccountId
}

async function getPlayerByName(playerName)
{
	const player = await riotData.fetchSummonerByName(dynamodb,playerName);
	await savePlayer(player);
	return player;
}

async function getPlayerByAccountId(accountId)
{
  //Try to fetch from DynamoDB
	let player = await fetchPlayerFromDB(accountId);
	if(!player)
	{
		player = savePlayer(await riotData.fetchSummonerByAccountId(dynamodb,accountId));
	}
	return player;
}

async function fetchPlayerFromDB(accountId)
{
	console.log("Creating fetch params");
	const params = {
		TableName: "lolcrawl-accounts",
		Key: { "accountId": accountId }
	};

	try{
		const playerData = await dynamodb.get(params).promise();
		console.debug(`Successfully fetched a player.`,playerData.Item);
		return playerData.Item;
	} catch(err){
		console.error(`Failed to fetch rate player ${accountId} from DynamoDB:`,err);
		throw err;
	}
}

async function savePlayer(player)
{
  console.log("Creating save params");
	const params = {
		TableName: "lolcrawl-accounts",
		Key: "accountId",
		Item:{
			accountId: player.accountId,
			name: player.name,
			lastUpdated: new Date().toISOString(),
			minGameFetched: -1,
			maxGameFetched: -1,
			minTimeFetched: -1,
			maxTimeFetched: -1
		},
	}

	try {
		const playerData = await dynamodb.put(params).promise();
		console.debug(`Successfully saved player.`,JSON.stringify(playerData));
		return playerData;
	} catch (err) {
		console.error(`Failed to put player into DynamoDB: ${player}:`,err);
		throw err;
	}
}
